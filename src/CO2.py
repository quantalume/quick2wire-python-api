#!/usr/bin/env python3

from quick2wire.i2c import I2CMaster, writing_bytes, reading
import time

address = 0x68

with I2CMaster() as master:    
    master.transaction(
        writing_bytes(address, 0x22, 0x00, 0x08, 0x2A))
        
    time.sleep(1)
 
    read_results = master.transaction(reading(address, 4))
    print(read_results)
    